/**
 * Created by Admin on 23/06/2016.
 */
import {Component, Input} from "angular2/core";
import {HeartComponent} from "./heart.component";
import {PersonnelService} from "./services/personnel.service";
import {PersonComponent} from "./person.component";
@Component({
    selector: 'persons',
    template: `
        <div *ngFor="#person of personnel">
            <person [data]="person"></person>
        </div>

`,
    providers: [PersonnelService],
    directives: [HeartComponent, PersonComponent]
})
export class PersonsComponent {

    personnel: any[];

    constructor(personnelService: PersonnelService) {
        this.personnel = personnelService.getPersonnel();
    }
}
