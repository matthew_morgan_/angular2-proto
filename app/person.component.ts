/**
 * Created by Admin on 23/06/2016.
 */
import {Component, Input} from "angular2/core";
import {HeartComponent} from "./heart.component";
@Component({
    selector: 'person',
    template: `
        <ul>
                <div class="media">
                    <div class="media-left">
                    <a href="#">
                    <img class="media-object" src="http://screenshots.en.sftcdn.net/en/scrn/69715000/69715548/star-trek-timelines-08-100x100.png" alt="File Image">
                    </a>
                 </div>
                <div class="media-body">
                    <h4 class="media-heading">{{ data.name }}</h4>
                        {{ data.description }} <heart></heart>
                    </div>
                </div>
        </ul>

`,
    directives: [HeartComponent]
})
export class PersonComponent {
    @Input() data;

    constructor(){
        console.log(this.data);
    }
}