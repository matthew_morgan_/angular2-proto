import {Component, Input} from "angular2/core";
/**
 * Created by Admin on 21/06/2016.
 */


@Component({
    selector: 'vote',
    template: `<div class="voteDiv">
                <span class="glyphicon glyphicon-menu-up" (click)="onUpvote()"></span>
                {{numberOfVotes}}
                <span class="glyphicon glyphicon-menu-down" (click)="onDownvote()"></span>
               </div>
                
              `,
    styles: [`
             div.voteDiv{
               max-width: 25px;
             }
             
               `]
})
export class VoteComponent {

    @Input() numberOfVotes;
    upVoted: boolean = false;
    downVoted: boolean = false;
    
    onUpvote(){
        console.log("Upboated");
        
        if(this.downVoted){
            ++this.numberOfVotes;
            this.downVoted = false;
        }
        if(!this.upVoted){
            ++this.numberOfVotes;
            this.upVoted = true;
            this.downVoted = false;
        }
    }

    onDownvote(){
        console.log("Downboated");

        if(this.upVoted){
            --this.numberOfVotes;
            this.upVoted = false;
        }
        if(!this.downVoted){
            --this.numberOfVotes;
            this.downVoted = true;
            this.upVoted = false;
        }

    }

}