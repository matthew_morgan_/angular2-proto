/**
 * Created by Admin on 14/06/2016.
 */

import {Component} from 'angular2/core';


@Component({
    selector: 'favourite-star',
    template: `<span 
                class="glyphicon"
                (click)="onClick()" 
                [class.glyphicon-star]="favourite"
                [class.glyphicon-star-empty]="!favourite">
                </span>`
})
export class FavouriteComponent {

    favourite: boolean;

    onClick(){
        this.favourite = !this.favourite;
        console.log("Star clicked dick");
    }

    constructor(){
        this.favourite = false;
    }

}