import {Component, Output, EventEmitter} from "angular2/core";
@Component({
    selector: 'heart',
    template: `<span 
               class="glyphicon glyphicon-heart"
               (click)="onClick()"
               [style.color]="hearted ? 'deeppink' : 'grey'"
               >
               </span>`,
    styles: [`
             .glyphicon-heart{
             cursor:pointer}
             `]

})
export class HeartComponent {
    
    hearted: boolean = false;
    @Output('heart-change') heartedEmitter = new EventEmitter();

    onClick(){
        this.hearted = !this.hearted;
        this.heartedEmitter.emit({newValue: this.hearted});
    }
}