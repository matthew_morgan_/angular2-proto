/**
 * Created by Admin on 11/06/2016.
 */
import{Component} from 'angular2/core';
import{InstructorService} from "./services/instructors.service";

@Component({
    selector: 'instructors',
    template: `
        <h2>Instructors</h2>
        {{title}}
        <ul>
            <li *ngFor="#instructor of instructors">
                {{instructor}}        
            </li>
        </ul>
        `,
    providers: [InstructorService]
})
export class InstructorsComponent {

    title:string;
    instructors:string[];

    constructor(instructorService: InstructorService) {
        this.instructors = instructorService.getInstructors();
    }

}
