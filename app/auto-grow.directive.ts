import {Directive, ElementRef, Renderer} from 'angular2/core';

@Directive({
    selector: '[autoGrow]',
    host: {
        '(focus)': 'onFocus()', //binds focus method to onFocus event
        '(blur)': 'onBlur()'
    }
})
export class AutoGrowDirective {
    
    constructor(private el: ElementRef, private renderer: Renderer){ // Private creates the private fields automagically
    }

    onFocus(){
        this.renderer.setElementStyle(this.el.nativeElement, 'width', '200');
    }

    onBlur(){
        this.renderer.setElementStyle(this.el.nativeElement, 'width', '150');
    }

}