/**
 * Created by Admin on 12/06/2016.
 */
System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var InstructorService;
    return {
        setters:[],
        execute: function() {
            InstructorService = (function () {
                function InstructorService() {
                }
                InstructorService.prototype.getInstructors = function () {
                    return ["Data", "Jean-Luc Picard", "William T. Riker"];
                    // Normally we'd make a RESTful API call here, rather than hardcoding the list
                };
                InstructorService.prototype.getTitle = function () {
                    return "Instructors";
                };
                return InstructorService;
            }());
            exports_1("InstructorService", InstructorService);
        }
    }
});
//# sourceMappingURL=instructors.service.js.map