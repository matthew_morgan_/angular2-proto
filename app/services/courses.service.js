/**
 * Created by Admin on 12/06/2016.
 */
System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var CourseService;
    return {
        setters:[],
        execute: function() {
            CourseService = (function () {
                function CourseService() {
                }
                CourseService.prototype.getCourses = function () {
                    return ["Advanced Metallurgy", "Stellar Cartography", "AI Psychiatry", "Cybernetics"];
                    // Normally we'd make a RESTful API call here, rather than hardcoding the list
                };
                CourseService.prototype.getTitle = function () {
                    return "Get learned";
                };
                return CourseService;
            }());
            exports_1("CourseService", CourseService);
        }
    }
});
//# sourceMappingURL=courses.service.js.map