/**
 * Created by Admin on 12/06/2016.
 */

export class CourseService {
    getCourses() : string[] {
        return ["Advanced Metallurgy", "Stellar Cartography", "AI Psychiatry", "Cybernetics"]; 
        // Normally we'd make a RESTful API call here, rather than hardcoding the list
    }
    
    getTitle() : string {
        return "Get learned";
    }
}