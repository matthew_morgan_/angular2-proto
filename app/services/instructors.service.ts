/**
 * Created by Admin on 12/06/2016.
 */

export class InstructorService {
    getInstructors() : string[] {
        return ["Data", "Jean-Luc Picard", "William T. Riker"];
        // Normally we'd make a RESTful API call here, rather than hardcoding the list
    }

    getTitle() : string {
        return "Instructors";
    }
}