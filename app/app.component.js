System.register(['angular2/core', "./courses.component", "./instructors.component", "./favourite.component", "./heart.component", "./vote.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, courses_component_1, instructors_component_1, favourite_component_1, heart_component_1, vote_component_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (courses_component_1_1) {
                courses_component_1 = courses_component_1_1;
            },
            function (instructors_component_1_1) {
                instructors_component_1 = instructors_component_1_1;
            },
            function (favourite_component_1_1) {
                favourite_component_1 = favourite_component_1_1;
            },
            function (heart_component_1_1) {
                heart_component_1 = heart_component_1_1;
            },
            function (vote_component_1_1) {
                vote_component_1 = vote_component_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent() {
                    this.numberOfHearts = 665; // should be passed to heart component
                    this.numberOfVotes = 776; // should be taken from a service
                }
                AppComponent.prototype.onClick = function ($event) {
                    console.log($event);
                };
                AppComponent.prototype.onHeartChange = function ($event) {
                    console.log($event);
                    this.numberOfHearts = $event.newValue == true ? ++this.numberOfHearts : --this.numberOfHearts;
                };
                AppComponent.prototype.onDivClick = function () {
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        template: "<h1>My First Angular 2 App</h1>\n               <courses></courses>\n               <instructors></instructors>\n               <div (click)=\"onDivClick()\">\n               <button (click)=\"onClick($event)\"> Submit </button>\n               <favourite-star></favourite-star>\n               <p>{{numberOfHearts}}\n               <heart (heart-change)=\"onHeartChange($event)\"></heart> \n               </p>\n               <vote [numberOfVotes]=\"numberOfVotes\"></vote>    \n               </div>",
                        directives: [courses_component_1.CoursesComponent,
                            instructors_component_1.InstructorsComponent,
                            favourite_component_1.FavouriteComponent,
                            heart_component_1.HeartComponent,
                            vote_component_1.VoteComponent]
                    }), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.component.js.map