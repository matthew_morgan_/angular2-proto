/**
 * Created by Admin on 11/06/2016.
 */
import{Component} from 'angular2/core';
import{CourseService} from "./services/courses.service";
import {AutoGrowDirective} from "./auto-grow.directive";

@Component({
    selector: 'courses',
    template: `
        <h2>Courses</h2>
        {{title}}
        <input type="text" autoGrow [(ngModel)]="title" />
        <ul>
            <li *ngFor="#course of courses">
                {{course}}        
            </li>
        </ul>
        `,
    providers: [CourseService],
    directives: [AutoGrowDirective]
})
export class CoursesComponent {

    title: string;
    courses: string[];

    constructor(courseService: CourseService) {
        this.courses = courseService.getCourses();
        this.title = courseService.getTitle();
    }


}