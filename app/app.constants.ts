/**
 * Created by Admin on 12/06/2016.
 */
import { Injectable } from 'angular2/core';

@Injectable()
export class Configuration {
    public Server: string = "http://localhost:8080/";
    public ApiUrl: string = "";
    public ServerWithApiUrl = this.Server + this.ApiUrl;
}