import {Component} from 'angular2/core';
import {CoursesComponent} from "./courses.component";
import {InstructorsComponent} from "./instructors.component"
import {FavouriteComponent} from "./favourite.component";
import {HeartComponent} from "./heart.component";
import {VoteComponent} from "./vote.component";
import {PersonComponent} from "./person.component";
import {PersonsComponent} from "./persons.component";

@Component({
    selector: 'my-app',
    template: `<h1>My First Angular 2 App</h1>
               <courses></courses>
               <instructors></instructors>
               <div (click)="onDivClick()">
               <button (click)="onClick($event)"> Submit </button>
               <favourite-star></favourite-star>
               <p>{{numberOfHearts}}
               <heart (heart-change)="onHeartChange($event)"></heart> 
               </p>
               <vote [numberOfVotes]="numberOfVotes"></vote>    
               <persons></persons>
               </div>`,

    directives: [CoursesComponent, 
                InstructorsComponent, 
                FavouriteComponent, 
                HeartComponent, 
                VoteComponent,
                PersonsComponent]
})
export class AppComponent {

    numberOfHearts = 665; // should be passed to heart component
    numberOfVotes = 776; // should be taken from a service
    
    onClick($event){
        console.log($event);
    }

    onHeartChange($event) {
        console.log($event);
        this.numberOfHearts = $event.newValue == true ? ++this.numberOfHearts : --this.numberOfHearts;
    }
    
    onDivClick(){
    }
}